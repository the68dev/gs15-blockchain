# GS15-Blockchain

Projet de GS15 visant à reproduire une blockchain simple et des outils de communication sécurisée.

## Lancer le projet
Le projet se lance avec python 3 (testé avec version 3.9).
```shell
python main.py
```

Une fois lancé, les commandes de l'interface s'appellent par leur numéro ou leur nom directement.
Il n'est jamais obligatoire de spécifier des arguments, ils seront redemandés si ils sont nécessaires,
sinon la valeur par défaut sera utilisée.

## Lancer les tests unitaires

Quelques tests unitaires ont été rédigés, on peut les lancer avec la commande suivante.
```shell
python -m unittest discover -s ./src/test -t ./src/test
```