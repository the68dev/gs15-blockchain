import secrets

from src.hash.martine_fait_du_hash import martine_fait_du_hash_____
from src.math.bytesutils import mask


def bob_l_eponge(internal_block_size=128, input_block_size=64):
    io_mask = mask(input_block_size)
    hash = 0

    inp = yield -1
    while True:
        hash = martine_fait_du_hash_____(inp ^ hash, internal_block_size)
        inp = yield hash & io_mask


def hash_string(content: str, hash_size=3):
    """Hash the given string used the internal algorithm,
    return a hash of length 64 * hash_size"""
    byte_content = content.encode("utf-8")
    block_size = 64
    block_byte_size = block_size // 8
    hasher = bob_l_eponge(input_block_size=block_size)
    next(hasher)
    for i in range(len(byte_content) // block_byte_size):
        slice = byte_content[i * block_byte_size: (i + 1) * block_byte_size]
        hasher.send(int.from_bytes(slice, byteorder='little'))

    remainder = len(byte_content) % block_byte_size
    if remainder > 0:
        last_slice = int.from_bytes(byte_content[-remainder:], byteorder='little')
        hasher.send(last_slice << (block_size - remainder * 8))

    hash = 0
    for i in range(hash_size):
        hash = (hash << block_size) + hasher.send(0)

    return hash


def hash_file(file_path: str, hash_size: int = 3):
    block_size = 64
    block_byte_size = block_size // 8
    hasher = bob_l_eponge(input_block_size=block_size)
    next(hasher)
    with open(file_path, 'rb') as f:
        temp = f.read(block_byte_size)
        while temp != b'':
            if len(temp) == 8:
                hasher.send(int.from_bytes(temp, byteorder='little'))
            elif len(temp) > 0:
                remainder = len(temp) * 8
                temp = int.from_bytes(temp, "little")
                hasher.send(temp << (block_size - remainder))
            temp = f.read(block_byte_size)

    hash = 0
    for i in range(hash_size):
        hash = (hash << block_size) + hasher.send(0)

    return hash


if __name__ == '__main__':
    data = [secrets.randbits(128) for i in range(256)]
    for _ in range(4):
        eponge = bob_l_eponge()
        next(eponge)

        for block in data:
            eponge.send(block)

        result = 0
        for _ in range(3):
            result = (result << 128) + eponge.send(0)

        print(hex(result))
        data[1] -= 1
