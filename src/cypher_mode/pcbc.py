from typing import Generator

def pcbc_of(generator: Generator[int, int, None], iv: int) -> Generator[int, int, None]:
    """
        Applique le mode PCBC au générateur fourni, chaque bloc sera chiffré en fonction du bloc
        précedent.
    """
    chunk = yield -1
    xor_key = iv

    next(generator)

    # Iteration sur le contenu recu
    while chunk >= 0:
        # Chiffrement du bloc
        crypted = generator.send(chunk ^ xor_key)

        # Le prochain précédant est le bloc en clair, xoré avec le résultat
        xor_key = chunk ^ crypted

        # On le renvoie et on récupère le bloc en clair suivant
        chunk = yield crypted


def pcbc_inv_of(generator: Generator[int, int, None], iv: int) -> Generator[int, int, None]:
    """
        Applique le mode PCBC inversé au générateur fourni, chaque bloc sera déchiffré en fonction du bloc
        précedent.
    """
    chunk = yield -1
    xor_key = iv

    next(generator)

    # Iteration sur le contenu recu
    while chunk >= 0:
        # Déchiffrement du bloc
        decrypted = generator.send(chunk) ^ xor_key

        # Le prochain précédant est le bloc en clair, xoré avec celui chiffré
        xor_key = chunk ^ decrypted

        # On le renvoie et on récupère le bloc en clair suivant
        chunk = yield decrypted