from typing import Generator


def cbc_of(generator: Generator[int, int, None], iv: int) -> Generator[int, int, None]:
    """
        Applique le mode CBC au générateur fourni, chaque bloc sera chiffré en fonction du bloc
        précedent.
    """
    chunk = yield -1
    previous = iv

    next(generator)
    # Iteration sur le contenu recu
    while chunk >= 0:
        # Le prochain précédent est le bloc chiffré actuel
        previous = generator.send(chunk ^ previous)

        # On le renvoie et on récupère le bloc suivant
        chunk = yield previous


def cbc_inv_of(generator: Generator[int, int, None], iv: int) -> Generator[int, int, None]:
    """
        Applique le mode CBC au générateur fourni, chaque bloc sera chiffré en fonction du bloc
        précedent.
    """
    chunk = yield -1
    previous = iv
    next(generator)
    # Iteration sur le contenu recu
    while chunk >= 0:
        orig_chunk = chunk
        # On le renvoie et on récupère le bloc suivant
        chunk = yield generator.send(chunk) ^ previous

        # Le prochain précédent est le bloc chiffré actuel
        previous = orig_chunk
