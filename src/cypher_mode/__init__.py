from typing import Generator


def ecb_of(generator: Generator[int, int, None]) -> Generator[int, int, None]:
    """Applique le mode ECB au générateur fourni, chaque bloc sera chiffré de la même manière."""

    # Mode ECB -> pas de liens entre les blocs, on obtient le générateur initial
    return generator
