import unittest
from typing import Generator, List


def xor_generator(key: int) -> Generator[int, int, None]:
    """
    Crée un générateur de bloc xoré avec la clef

    :param key: cypher key
    """
    chunk = yield -1

    # Iteration sur le contenu recu
    while chunk >= 0:
        chunk = yield chunk ^ key


class CipherGeneratorFunctionsTestCase(unittest.TestCase):

    def assertReversibleCipherGenerator(self, generator: Generator[int, int, None],
                                        generator_rev: Generator[int, int, None],
                                        chunks: List[int]):
        encoded = [0] * len(chunks)
        decoded = [0] * len(chunks)

        # Crypt block
        next(generator)
        for i in range(len(chunks)):
            encoded[i] = generator.send(chunks[i])

        generator.close()

        # Decrypt block
        next(generator_rev)
        for i in range(len(chunks)):
            decoded[i] = generator_rev.send(encoded[i])
        generator_rev.close()

        self.assertListEqual(decoded, chunks)
