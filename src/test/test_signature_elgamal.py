import unittest
from src.signature.ElGamalEnc import ElGamalEnc
from src.hash.bob_l_eponge import *


class ElGamalSignTestCase(unittest.TestCase):
    lst = ['Ranit', 'sautes', 'snow-thrower', 'swore',
           'Turkistan', 'unwillingness', 'Zwinglianism']

    def test_sign_values(self):
        for ele in self.lst:
            elgam = ElGamalEnc(128)
            hach = hash_string(ele)
            sign = elgam.sign(hach)
            self.assertTrue(elgam.verify(hach, sign))

    def test_sign_values_2(self):
        sigs = set()
        elgam = ElGamalEnc(64)
        for ele in self.lst:
            hach = hash_string(ele)
            sig = elgam.sign(hach)
            sigs.add(sig)
        self.assertEqual(len(sigs), len(self.lst))


if __name__ == '__main__':
    unittest.main()
