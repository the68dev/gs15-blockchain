import unittest
from math import sqrt
from src.signature.prime import find_prime, find_generator


class PrimeSearchTestCase(unittest.TestCase):
    def test_find_prime(self):
        for _ in range(10):
            prime = find_prime(15)
            for value in range(2, int(sqrt(prime)) + 1):
                self.assertNotEqual(prime % value, 0, f"{prime} has {value} as a divisor")

    def test_find_generator(self):
        for _ in range(10):
            prime = find_prime(15)
            alpha = find_generator(prime)
            current = 1
            for i in range(prime - 2):
                current = (alpha * current) % prime
                self.assertNotEqual(current, 1, f"{alpha} only generates {i+1} elements mod {prime}")



if __name__ == '__main__':
    unittest.main()
