import unittest

from src.math.modularutils import modular_inverse


class ModularUtilsTestCase(unittest.TestCase):
    def test_modular_inverse(self):
        self.assertEqual(modular_inverse(23, 120), 47)
        self.assertEqual(modular_inverse(13, 7), 6)
