import unittest
import secrets
from src.kasumi.sbox import reverse_sbox, generate_sbox
from src.kasumi import kasumi, kasumi_inv
from src.test.utils import CipherGeneratorFunctionsTestCase

class SboxTestCase(unittest.TestCase):
    def test_reversible_sbox(self):
        for i in range(200):
            key = secrets.randbits(128)
            sbox = generate_sbox(key, 256, 128)
            rev = reverse_sbox(sbox)

            for j in range(100):
                block = secrets.randbits(8)

                self.assertEqual(block, sbox[rev[block]])
                self.assertEqual(block, rev[sbox[block]])

    def test_consistent_sbox(self):
        for i in range(50):
            key = secrets.randbits(128)
            sbox1 = generate_sbox(key, 256)
            sbox2 = generate_sbox(key, 256)

            self.assertEqual(sbox1, sbox2)

    def test_different_sbox(self):
        for i in range(50):
            key = secrets.randbits(124) << 4
            key_alt = (secrets.randbits(124) << 4) | 0xE
            sbox1 = generate_sbox(key, 256)
            sbox2 = generate_sbox(key_alt, 256)

            self.assertNotEqual(sbox1, sbox2)


class KasumiFunctionsTestCase(CipherGeneratorFunctionsTestCase):
    def test_reversible_kasumi(self):
        for i in range(200):
            key = secrets.randbits(128)
            chunks = [secrets.randbits(64), secrets.randbits(64)]
            self.assertReversibleCipherGenerator(kasumi(key), kasumi_inv(key), chunks)

if __name__ == '__main__':
    unittest.main()
