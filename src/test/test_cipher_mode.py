import secrets
from src.test.utils import CipherGeneratorFunctionsTestCase, xor_generator

from src.cypher_mode.cbc import cbc_of, cbc_inv_of
from src.cypher_mode.pcbc import pcbc_of, pcbc_inv_of


class CipherModeTestCase(CipherGeneratorFunctionsTestCase):
    def test_XOR_generator(self):
        key = secrets.randbits(128)
        generator = xor_generator(key)
        generator_rev = xor_generator(key)
        chunks = [secrets.randbits(128) for _ in range(10)]
        self.assertReversibleCipherGenerator(generator, generator_rev, chunks)

    def test_reversible_CBC_mode(self):
        for i in range(200):
            key = secrets.randbits(128)
            iv = secrets.randbits(128)
            generator = cbc_of(xor_generator(key), iv)
            generator_rev = cbc_inv_of(xor_generator(key), iv)
            chunks = [secrets.randbits(128) for _ in range(10)]
            self.assertReversibleCipherGenerator(generator, generator_rev, chunks)

    def test_reversible_PCBC_mode(self):
        for i in range(200):
            key = secrets.randbits(128)
            iv = secrets.randbits(128)
            generator = pcbc_of(xor_generator(key), iv)
            generator_rev = pcbc_inv_of(xor_generator(key), iv)
            chunks = [secrets.randbits(128) for _ in range(10)]
            self.assertReversibleCipherGenerator(generator, generator_rev, chunks)

