import unittest
from src.hash.martine_fait_du_hash import martine_fait_du_hash_____

class HashTestCase(unittest.TestCase):
    def test_hash_values(self):
        values = set()
        for i in range(pow(2, 16)):
            hash = martine_fait_du_hash_____(i, 16)
            values.add(hash)
        self.assertGreater(len(values), pow(2, 16)*0.9, "non reliable hash method")

if __name__ == '__main__':
    unittest.main()
