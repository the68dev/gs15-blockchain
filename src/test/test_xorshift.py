import secrets
import unittest
from src.math.xoshiro import Xoshiro


class XoshiroTestCase(unittest.TestCase):
    def test_not_identical(self):
        x = Xoshiro(secrets.randbits(256))
        nb_loop = 64
        s = set()
        for _ in range(nb_loop):
            s.add(x.next())
        self.assertEqual(len(s), 64)
