import secrets
import unittest

from src.hash.bob_l_eponge import bob_l_eponge, hash_string


class SpongeTestCase(unittest.TestCase):
    lst = ['2', 'Ardyth', 'boletes', 'chrisom', 'cureless', 'durrin', 'fishybacking', 'guiltless', 'immunoglobulin',
           'Kutenai', 'menacing', 'non-Calvinistic', 'ovoplasm', 'poligar', 'Ranit', 'sautes', 'snow-thrower', 'swore',
           'Turkistan', 'unwillingness', 'Zwinglianism']
    def test_sponge_values_equal(self):
        for _ in range(10):
            data = [secrets.randbits(128) for i in range(256)]
            self.assertEqual(self.sponge_wrapper(data), self.sponge_wrapper(data), "Not equals values")

    def test_sponge_values_differents(self):
        hsh = {hash_string(elem) for elem in self.lst}
        self.assertEqual(len(hsh), len(self.lst), "two hash are equals")



    def sponge_wrapper(self, data):
        eponge = bob_l_eponge()
        next(eponge)

        for block in data:
            eponge.send(block)

        result = 0
        for i in range(3):
            result = (result << 128) + eponge.send(0)

        return hex(result)

if __name__ == '__main__':
    unittest.main()
