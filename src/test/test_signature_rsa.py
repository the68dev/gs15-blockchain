import unittest
from src.signature.RSAEnc import RSAEnc
from src.hash.bob_l_eponge import *


class RSASignTestCase(unittest.TestCase):
    lst = ['Ranit', 'sautes', 'snow-thrower', 'swore',
           'Turkistan', 'unwillingness', 'Zwinglianism']

    def test_sign_values(self):
        for ele in self.lst:
            rsa = RSAEnc(64)
            hach = hash_string(ele)
            sign = rsa.sign(hach)
            self.assertTrue(rsa.verify(hach, sign))

    def test_sign_values_2(self):
        sigs = set()
        rsa = RSAEnc(64)
        for ele in self.lst:
            hach = hash_string(ele)
            sig = rsa.sign(hach)
            sigs.add(sig)
        self.assertEqual(len(sigs), len(self.lst))


if __name__ == '__main__':
    unittest.main()
