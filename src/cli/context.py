import pickle
from typing import Union, List, Dict
from datetime import datetime
from os import path, mkdir

from src.blockchain.blockchain import Blockchain
from src.blockchain.user import User

CONTEXT_DIR = ".context"


class Context:
    def __init__(self):
        self.kazumi_key: Union[None, int] = None
        self.mode: Union[None, List] = None
        self.users: Dict[str, User] = {}
        self.last_hash: Union[None, int] = None
        self.blockchains: Dict[str, Blockchain] = {}
        self.saving_date: datetime = datetime.now()

    def save(self):
        self.saving_date = datetime.now()
        with open(f"{CONTEXT_DIR}/latest.dump", "wb+") as file:
            pickle.dump(self, file)

    @staticmethod
    def load():
        if not path.exists(f"{CONTEXT_DIR}/latest.dump"):
            if not path.exists(CONTEXT_DIR):
                mkdir(CONTEXT_DIR)
            return Context()

        with open(f"{CONTEXT_DIR}/latest.dump", "rb") as file:
            context: Context = pickle.load(file)
            date = context.saving_date

            # Save backup
            with open(f"{CONTEXT_DIR}/" + \
                      f"{date.year}-{date.month}-{date.day}-{date.hour}-{date.minute}-{date.second}.dump", "wb+") as output:
                pickle.dump(context, output)

            return context

    def description_for_menu(self, menu_name: str):
        content = []
        if menu_name == "crypt":
            if self.kazumi_key is not None:
                content.append(f"kasumi key = {hex(self.kazumi_key)}")

            if self.mode is not None:
                content.append(f"mode = {self.mode[0] if self.mode is not None else self.mode}")

        elif menu_name == "hash" and self.last_hash is not None:
            content = [f"last hash = {hex(self.last_hash)}"]

        elif menu_name == "blockchain":
            if len(self.users) > 0:
                content.append("users = { " + ", ".join([name for name in self.users]) + " }")

            if len(self.blockchains) > 0:
                content.append("blockchains = { " + ",".join([name for name in self.blockchains]) + " }")

        if len(content) == 0:
            return None
        else:
            return "[ " + ", ".join(content) + " ]"

    def __str__(self):
        return f"[, , {len(self.users)} users, " \
               f"last hash = {self.last_hash}] "
