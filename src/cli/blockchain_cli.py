import secrets

from src.blockchain.blockchain import Blockchain, Transaction
from src.blockchain.user import User
from src.cli.command import Command, CLI, Arg
from src.cli.context import Context
from src.hash.bob_l_eponge import hash_string
from src.math.bytesutils import mask
from src.signature.ElGamalEnc import ElGamalEnc
from src.signature.RSAEnc import RSAEnc


class NoBlockchainDefinedError(Exception):
    pass


class NoUsersDefinedError(Exception):
    pass


def create_user(context: Context, name: str, sign_algorithm: str, key_size: int):
    while sign_algorithm not in ["rsa", "el_gm"]:
        sign_algorithm = input("signature algorithm [rsa / el_gm]: ")

    print(f"generating private and public keys of size {key_size}...")
    user = User(name, ElGamalEnc(key_size) if sign_algorithm == "el_gm" else RSAEnc(key_size))

    context.users[user.name] = user


def show_users(context: Context):
    for key in context.users:
        print(context.users[key])


def input_user(context: Context, message: str):
    if len(context.users) == 0:
        raise NoUsersDefinedError()

    name = input(f"{message} = ")
    while name not in context.users:
        print("user not found")
        name = input(f"{message} = ")

    return context.users[name]


def input_blockchain(context: Context, current_name):
    if len(context.blockchains) == 0:
        raise NoBlockchainDefinedError()

    while current_name not in context.blockchains:
        for key in context.blockchains:
            print(f"- {key}")
        current_name = input("blockchain = ")

    return context.blockchains[current_name]


def make_proof_of_work(_: Context, zeros: int, content: str):
    print(f"finding a hash ending with {zeros} zeros")
    end_mask = mask(zeros)

    nonce = secrets.randbits(100)
    hash = hash_string(content + " ~ " + hex(nonce))
    iteration = 1
    while hash & end_mask != 0:
        hash = hash_string(content + " ~ " + hex(nonce))
        iteration += 1
        nonce += 1
        if iteration % 10000 == 0:
            print(f"iteration {iteration}...")

    print(f"\nfound nonce = {hex(nonce)}")
    print(f"hash({content} ~ {hex(nonce)}) = {bin(hash)}\n")


def check_transaction(context: Context, blockchain_name: str, transaction_id: int):
    # Variable checking
    blockchain = input_blockchain(context, blockchain_name)

    last_transaction = blockchain.transactions_count() - 1
    while transaction_id < 0 or transaction_id > last_transaction:
        transaction_id = input(f"transaction [0-{last_transaction}] = ")
        while not transaction_id.isnumeric():
            transaction_id = input(f"transaction [0-{last_transaction}] = ")
        transaction_id = int(transaction_id)

    transaction = blockchain.transaction(int(transaction_id))
    transaction.check_verbose()


def insert_into(context: Context, blockchain_name: str):
    if blockchain_name not in context.blockchains:
        context.blockchains[blockchain_name] = Blockchain()
    blockchain = context.blockchains[blockchain_name]

    transactions = []
    try:
        print("reading all transactions, press Ctrl+C to end")
        while True:
            source = input_user(context, "from")
            destination = input_user(context, "to")
            amount = input("transaction amount = ")
            while not amount.isnumeric():
                amount = input("transaction amount = ")
            amount = float(amount)

            transactions.append(Transaction(source, destination, amount))
    except KeyboardInterrupt as e:
        pass

    if len(transactions) > 0:
        blockchain.add(transactions)
    else:
        print("no transactions to be added, skipping...")


def check_blockchain(context: Context, blockchain_name: str):
    blockchain = input_blockchain(context, blockchain_name)
    blockchain.check_verbose()


blockchain_cli = CLI([
    Command(
        "create_user",
        "Create new user",
        args=[Arg("name", str), Arg("sign_algorithm", str, "rsa"), Arg("key_size", int, "128")],
        method=create_user
    ),
    Command(
        "users",
        "Display all created users",
        args=[],
        method=show_users
    ),
    Command("check_transaction", "Check a transaction", args=[Arg("blockchain", str), Arg("transaction_id", int)],
            method=check_transaction),
    Command(
        "insert",
        "Insert a new block into a blockchain",
        args=[Arg("blockchain", str)],
        method=insert_into
    ),
    Command(
        "pof",
        "Make a proof of work",
        args=[Arg("zeros", int, "4"), Arg("content", str)],
        method=make_proof_of_work
    ),
    Command(
        "check",
        "Check a blockchain",
        args=[Arg("blockchain", str)],
        method=check_blockchain
    )
])
