from collections.abc import Callable
from math import ceil, floor
from typing import List, Type

from src.cli.context import Context


class Arg:
    def __init__(self, name: str, arg_type: Type, default: str = None):
        self.name = name
        self.type = arg_type
        self.default = default

    def __str__(self):
        return f"{self.name} ({self.type.__name__}){' = ' + self.default if self.default is not None else ''}"


class Command:
    def __init__(self, name: str, description: str, args: List[Arg], method: Callable):
        self.name = name
        self.args = args
        self.description = description
        self.method = method

    def __str__(self):
        args_str = ", ".join(map(str, self.args))
        return f"{self.name} {{ {args_str} }}\n\t{self.description}"


class CLI:
    def __init__(self, commands: List[Command]):
        self.commands = commands
        self.help_text = None

    def display_commands(self):
        if self.help_text is None:
            title = "[ Available commands ]"
            commands = "\n".join([f"[{i + 1}] {self.commands[i]}" for i in range(len(self.commands))])
            longest_line = max(map(lambda x: len(x), commands.split("\n")))
            side_length = (longest_line - len(title)) / 2
            self.help_text = f"{'=' * floor(side_length)}{title}{'=' * ceil(side_length)}\n" \
                             + commands + "\n" \
                             + '=' * longest_line + "\n"

        print(self.help_text)

    def loop(self, context: Context, prefix: str = ""):
        self.display_commands()
        while self.ask(context, prefix):
            context.save()

    def ask(self, context: Context, prefix: str = "") -> bool:
        """Ask the user a command and run it, returns whether the program should continue"""
        context_text = context.description_for_menu(prefix)
        if context_text is not None:
            print(context_text)

        query = input(prefix + "> ").split()

        if query[0] == "help":
            self.display_commands()
        elif query[0] == "exit":
            return False
        else:
            command = None

            if query[0].isnumeric() and int(query[0]) - 1 < len(self.commands):
                command = self.commands[int(query[0]) - 1]
            else:
                for it in self.commands:
                    if it.name == query[0]:
                        command = it

            if command is None:
                print("Command not found, type 'help' for a list of commands.")
            elif self.run_command(command, args=query[1:], context=context):
                self.display_commands()

        return True

    def run_command(self, command: Command, args: List[str], context: Context):
        # Ensure type match
        for i in range(len(args)):
            args[i] = command.args[i].type(args[i])

        # Make sure arguments matches
        while len(args) < len(command.args):
            current_arg = command.args[len(args)]
            # Si une valeur par défaut n'est pas spécifiée
            if current_arg.default is None:
                # On la demande
                while True:
                    # Catch input errors
                    try:
                        args.append(current_arg.type(input(f"{current_arg.name} = ")))
                    except ValueError:
                        continue
                    break
            else:
                # Sinon on prend la valeur par défaut
                args.append(current_arg.type(current_arg.default))

        # Run command
        try:
            return command.method(context, *args)
        except Exception as e:
            if isinstance(e, KeyboardInterrupt):
                raise e
            else:
                print(f"\n[! caught {e.__class__.__name__} !]\n")
