from src.cli.command import Command, CLI, Arg
from src.cli.context import Context
from src.cypher_mode import ecb_of
from src.cypher_mode.cbc import cbc_of, cbc_inv_of
from src.cypher_mode.pcbc import pcbc_of, pcbc_inv_of
from src.hash.bob_l_eponge import hash_string
from src.kasumi.crypt_file import decypher_kasumi_file, cypher_kasumi_file
from src.kasumi.diffie_hellman import diffie_hellman_exchange


def set_pass(context: Context, password: str):
    context.kazumi_key = hash_string(password, 2)


def exchange_key(context: Context, key_size: int):
    context.kazumi_key = diffie_hellman_exchange(key_size)


def set_mode(context: Context, mode: str) -> bool:
    mode = mode.strip().lower()

    if mode == "ecb":
        context.mode = [mode, ecb_of, ecb_of, False]
    elif mode == "cbc":
        context.mode = [mode, cbc_of, cbc_inv_of, True]
    elif mode == "pcbc":
        context.mode = [mode, pcbc_of, pcbc_inv_of, True]
    else:
        print("invalid cypher mode, choose between ecb, cbc or pcbc")
        return False

    return True


def cypher_kasumi_command(context: Context, source: str, dest: str):
    if context.kazumi_key is None:
        password = input("password = ")
        set_pass(context, password)

    while context.mode is None:
        mode = input("mode = ")
        set_mode(context, mode)

    if dest == "None" or source == dest:
        dest = source + ".enc"

    print(f"encrypting {source} into {dest}...\n")
    cypher_kasumi_file(source, dest, context.kazumi_key)


def decypher_kasumi_command(context: Context, source: str, dest: str):
    if context.kazumi_key is None:
        password = input("password = ")
        set_pass(context, password)

    while context.mode is None:
        mode = input("mode = ")
        set_mode(context, mode)

    if dest == "None" or source == dest:
        dest = source + ".dec"

    print(f"decrypting {source} into {dest}...\n")
    decypher_kasumi_file(source, dest, context.kazumi_key)


crypt_cli = CLI([
    Command(
        "encode",
        "Crypt a file using kasumi",
        args=[Arg("source", str), Arg("dest", str, "None")],
        method=cypher_kasumi_command
    ),
    Command(
        "decode",
        "Decrypt a file using kasumi",
        args=[Arg("source", str), Arg("dest", str, "None")],
        method=decypher_kasumi_command
    ),

    Command(
        "mode",
        "Set crypt mode (ECB, CBC, PCBC)",
        args=[Arg("mode", str)],
        method=set_mode
    ),
    Command("pass",
            "Set kasumi key using hash of a string",
            args=[Arg("password", str)],
            method=set_pass
    ),
    Command("key_exchange",
            "Set kasumi key using a new diffie-hellman key exchange",
            args=[Arg("key_length", int, "128")],
            method=exchange_key
    )
])
