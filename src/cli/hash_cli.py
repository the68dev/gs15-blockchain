import re

from src.cli.command import CLI, Command, Arg
from src.cli.context import Context
from src.hash.bob_l_eponge import hash_string, hash_file


def hash_file_content(context: Context, path: str):
    context.last_hash = hash_file(path)
    print("hash = " + hex(context.last_hash))


def hash_string_content(context: Context, content: str):
    context.last_hash = hash_string(content)
    print("hash = " + hex(context.last_hash))


def try_parse_hex(content: str):
    try:
        return int(content, 16)
    except ValueError:
        return None


def hash_from_context(context: Context, given_hash: str):
    if given_hash == '' and context.last_hash is not None:
        return context.last_hash

    hash = try_parse_hex(input("hash = "))
    while hash is None:
        hash = try_parse_hex(input("hash = "))

    return hash


def print_verification(context: Context, actual_hash: int, provided_hash: str):
    hash = hash_from_context(context, provided_hash)
    print(f"\nexpected = {hex(hash)}")
    print(f"actual   = {hex(actual_hash)}")
    print("hash " + ("matches" if actual_hash == hash else "does not match"))
    print()


def verify_file_hash(context: Context, file_path: str, hash: str):
    print_verification(context, hash_file(file_path), hash)


def verify_text_hash(context: Context, content: str, hash: str):
    print_verification(context, hash_string(content), hash)


hash_cli = CLI([
    Command("hash_file",
            'Hash file content',
            args=[Arg("path", str)],
            method=hash_file_content
            ),
    Command("hash_text",
            "Hash string content",
            args=[Arg("content", str)],
            method=hash_string_content),
    Command("check_file",
            "Check file hash matches given hash",
            args=[Arg("path", str), Arg("hash", str, "")],
            method=verify_file_hash),
    Command("check_text",
            "Check text hash matches given hash",
            args=[Arg("content", str), Arg("hash", str, "")],
            method=verify_text_hash)
])
