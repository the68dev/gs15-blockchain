from src.math.bytesutils import chunks_of, rotate_left
from .sbox import generate_sbox

class Keys:
    def __init__(self, key: int):
        # Split key
        self._keys = chunks_of(key, 128, 16)
        self._altkeys = chunks_of(key ^ 0x123456789ABCDEFFEDCBA9876543210, 128, 16)

        # Sbox
        self.sbox = generate_sbox(key)

        # Init function keys
        self.ko = []
        self.kl = []
        self.ki = []
        for i in range(8):
            self.kl.append([
                rotate_left(self._keys[i], 16, 1),
                self.altkey(i + 2)
            ])

            self.ko.append([
                rotate_left(self.key(i + 1), 16, 5),
                rotate_left(self.key(i + 5), 16, 8),
                rotate_left(self.key(i + 6), 16, 13)
            ])

            self.ki.append([
                self.altkey(i + 4),
                self.altkey(i + 3),
                self.altkey(i + 7)
            ])

    def key(self, index: int):
        return self._keys[index % 8]

    def altkey(self, index: int):
        return self._altkeys[index % 8]
