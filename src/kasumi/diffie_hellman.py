import secrets

from src.signature.prime import find_prime, find_generator, expo_rapide


def diffie_hellman_exchange(key_length=128):
    print("initiating a diffie hellman key exchange")

    p = find_prime(key_length)
    g = find_generator(p)
    print(f"p = {p}, g = {g}")

    a = secrets.randbelow(p)
    b = secrets.randbelow(p)
    print(f"\nuser private keys:\na = {a}\nb = {b}")

    A = expo_rapide(g, a, p)
    B = expo_rapide(g, b, p)
    print(f"\nuser public keys:\nA = {A}\nB = {B}")

    print(f"\nshared key:\n"
          f"A^b mod p = {expo_rapide(A, b, p)}\n"
          f"B^a mod p = {expo_rapide(B, a, p)}")

    return expo_rapide(A, b, p)


if __name__ == '__main__':
    diffie_hellman_exchange()
