from typing import List


def generate_sbox(key: int, size_sbox=256, key_size=128) -> List[int]:
    sbox = list(range(size_sbox))
    j = 0
    for i in range(size_sbox):
        key_part = (key >> ((8 * i) % key_size)) & 0xFF
        j = (j + sbox[i] + key_part) % size_sbox
        sbox[i], sbox[j] = sbox[j], sbox[i]
    return sbox


def reverse_sbox(sbox: List[int]) -> List[int]:
    length = len(sbox)
    result = [0] * length

    for i in range(length):
        result[sbox[i]] = i

    return result
