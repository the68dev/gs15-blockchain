from typing import List
from .keys import Keys
from src.math.bytesutils import rotate_left


def fi(left: int, key: int, sbox: List[int]) -> int:
    return (left >> 2) ^ ((sbox[key >> 8] << 8) + sbox[key & 0xFF])


def fl(data: int, keys: Keys, iteration: int):
    left = data >> 16
    right = data & 0xFFFF
    right = rotate_left(left & keys.kl[iteration][0], 16, 1) ^ right
    left = rotate_left(right | keys.kl[iteration][1], 16, 1) ^ left
    return (left << 16) + right


def fo(data: int, keys: Keys, iteration: int) -> int:
    """
        Fonction FO de kazumi, prends un bloc de 32 bytes en entrée
        et les clefs de chiffrement

        Renvoie le bloc chiffré
    """
    left = data >> 16
    right = data & 0xFFFF

    for i in range(3):
        left = right
        right = right ^ fi(
            right ^ keys.ko[iteration][i],
            keys.ki[iteration][i],
            keys.sbox
        )

    return (left << 16) + right
