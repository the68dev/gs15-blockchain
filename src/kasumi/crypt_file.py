import secrets
from src.kasumi import kasumi, kasumi_inv
from src.cypher_mode import *
from src.math.bytesutils import bytes_to_int, int_to_bytes


def cypher_kasumi_file(source: str, dest: str, key: int):
    with open(source, 'rb') as entry, open(dest, 'wb') as result:
        gen = ecb_of(kasumi(key))
        next(gen)
        chunk = entry.read(8)
        while chunk != b'':
            result.write(int_to_bytes(gen.send(bytes_to_int(chunk)), 8))
            chunk = entry.read(8)
        gen.close()


def decypher_kasumi_file(source: str, dest: str, key: int):
    with open(source, 'rb') as input_file, open(dest, 'wb') as output_file:
        gen = ecb_of(kasumi_inv(key))
        next(gen)
        chunk = input_file.read(8)
        zeros = 0
        while chunk != b'':
            # Decrypt data
            dec_bytes = int_to_bytes(gen.send(bytes_to_int(chunk)), 8)

            # If we previously stripped zeros unnecessarily
            if any(dec_bytes):
                # Append them back
                output_file.write(bytes([0] * zeros))
                zeros = 0

            # Strip zeros at the end of the file (padding added during encryption)
            while len(dec_bytes) > 0 and dec_bytes[len(dec_bytes) - 1] == 0:
                zeros += 1
                dec_bytes = dec_bytes[:-1]

            output_file.write(dec_bytes)
            chunk = input_file.read(8)
        gen.close()


if __name__ == "__main__":
    k = secrets.randbits(128)
    print(k)
    cypher_kasumi_file("../../test", "../../test.enc", k)
