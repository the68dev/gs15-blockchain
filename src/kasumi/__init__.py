from typing import Generator
from .functions import fo, fl
from .keys import Keys

def kasumi(key: int) -> Generator[int, int, None]:
    """
    Crée un générateur de bloc chiffré suivant l'algorithme KASUMI

    :Exemple :
    >>> gen = kasumi(some_128b_key)
    >>> next(gen) # start generator
    >>> result[0] = gen.send(data[0]) # first block (no data received)
    >>> result[1] = gen.send(data[1]) # n-th block, crypted n-1 block received
    >>> gen.close() # signal end

    :param key: cypher key
    """
    # Key scheduling
    keys = Keys(key)

    chunk = yield -1

    # Iteration sur le contenu recu
    while chunk >= 0:
        left = chunk >> 32
        right = chunk & 0xFFFFFFFF

        for iteration in range(8):
            old_right = right
            right = left

            # Si l'itération est impaire, on utilise fo . fl
            if (iteration + 1) % 2 == 1:
                left = old_right ^ fo(fl(left, keys, iteration), keys, iteration)
            else:
                # Sinon fl . fo
                left = old_right ^ fl(fo(left, keys, iteration), keys, iteration)

        # On renvoie le résultat, puis on récupère le bloc suivant
        chunk = yield (left << 32) + right


def kasumi_inv(key: int) -> Generator[int, int, None]:
    """
    Crée un générateur de bloc déchiffré suivant l'algorithme KASUMI

    :Exemple :
    >>> gen = kasumi(some_128b_key)
    >>> next(gen) # start generator
    >>> result[0] = gen.send(data[0]) # first block (no data received)
    >>> result[1] = gen.send(data[1]) # n-th block, crypted n-1 block received
    >>> gen.close() # signal end

    :param key: cypher key
    """
    # Key scheduling
    keys = Keys(key)

    chunk = yield -1

    # Iteration sur le contenu recu
    while chunk >= 0:
        left = chunk >> 32
        right = chunk & 0xFFFFFFFF

        for iteration in reversed(range(8)):
            old_left = left
            left = right

            # Si l'itération est impaire, on utilise fo . fl
            if (iteration + 1) % 2 == 1:
                right = old_left ^ fo(fl(right, keys, iteration), keys, iteration)
            else:
                # Sinon fl . fo
                right = old_left ^ fl(fo(right, keys, iteration), keys, iteration)

        # On renvoie le résultat, puis on récupère le bloc suivant
        chunk = yield (left << 32) + right
