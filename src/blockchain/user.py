import pickle
import secrets

from src.signature.ElGamalEnc import ElGamalEnc
from src.signature.signature import Signature


class User:
    name: str
    sign: Signature

    def __init__(self, name: str, sign: Signature):
        self.name = name
        self.sign = sign

    def __str__(self):
        sign = "\n\t".join(str(self.sign).split(", "))
        line_ret = '\n\t' if len(sign) > 0 else ' '
        return f"{self.name} {{{line_ret}{sign}\n}}"


if __name__ == '__main__':
    user = User("bob", ElGamalEnc(10))
    copy = pickle.loads(pickle.dumps(user))
    print(user.sign.public_key, copy.sign.public_key)
