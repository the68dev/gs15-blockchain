from datetime import datetime
from typing import List

from src.blockchain.user import User
from src.hash.bob_l_eponge import hash_string
from src.math.bytesutils import mask

import secrets

def indent(text: str, amount=1):
    lines = text.split("\n")

    if len(lines) == 0:
        return text

    return "\n".join(["  " * amount + line for line in lines])


class Transaction:
    source: User
    destination: User
    signature: int
    amount: float

    def __init__(self, source: User, destination: User, amount: float):
        self.source = source
        self.destination = destination
        self.amount = amount

        self.signature = source.sign.sign(hash_string(self.signed_content()))

    def signed_content(self):
        return f"{self.source.name}->{self.destination.name} : {self.amount}"

    def __str__(self):
        signature = hex(self.signature) if type(self.signature) is not tuple else "(" + ", ".join(map(hex, self.signature)) + ")" 
        return f"{self.signed_content()} [{signature}]"

    def check_verbose(self):
        signed_content = self.signed_content()
        hach = hash_string(signed_content)

        print("[ transaction content ]")
        print(indent(signed_content))
        print("[ signature verification ]")
        print(indent("signature = " + hex(self.signature) if type(self.signature) is not tuple else "(" + ", ".join(map(hex, self.signature)) + ")"))
        print(indent(f"hash of content = {hex(hach)}"))
        print(indent(f"verification = {self.source.sign.verify(hach, self.signature)}"))
        print()


class Block:
    hash: int
    previous_hash: int
    data: List[Transaction]
    timestamp: int
    nonce: int

    def __init__(self, data: List[Transaction], previous_hash: int, timestamp: int):
        self.data = data
        self.previous_hash = previous_hash
        self.timestamp = timestamp
        self.nonce = secrets.randbits(32)

        suffix_mask = mask(self.suffix_length())
        expected_suffix = self.expected_suffix()
        self.hash = hash_string(self.hash_content())
        while (self.hash & suffix_mask) ^ expected_suffix != 0:
            self.nonce += 1
            self.hash = hash_string(self.hash_content())

    @staticmethod
    def suffix_length():
        return 3

    def expected_suffix(self):
        return mask(self.suffix_length()) & (self.previous_hash >> self.suffix_length())

    def hash_content(self):
        return f"previous hash={hex(self.previous_hash)[0:16]}\n" + \
               f"timestamp={self.timestamp}\n" + \
               f"nonce={self.nonce}\n" + \
               f"[{len(self.data)} transactions]\n  " + \
               "\n  ".join([str(transaction) for transaction in self.data])

    def __str__(self):
        return f"hash = {hex(self.hash)}\ntransactions = {len(self.data)}" + \
               ("\n\t" if len(self.data) > 0 else "") + \
               "\n\t".join([str(transaction) for transaction in self.data])


class Blockchain:
    blocks: List[Block]

    def __init__(self):
        self.blocks = []

    def add(self, data: List[Transaction]):
        last_hash = self.blocks[-1].hash if len(self.blocks) > 0 else 0
        self.blocks.append(
            Block(data, last_hash, int(datetime.timestamp(datetime.now()) * 1000))
        )

    def transaction(self, i: int):
        """Find transaction of index i"""
        for block in self.blocks:
            if len(block.data) <= i:
                i -= len(block.data)
            else:
                return block.data[i]

        return None

    def transactions_count(self):
        count = 0
        for block in self.blocks:
            count += len(block.data)
        return count

    def check_verbose(self):
        previous_hash = 0
        for i in range(len(self.blocks)):
            print(f"[[ checking block {i} ]]")
            print(f"  [block content]")
            block = self.blocks[i]
            print(indent(block.hash_content(), 2))
            print("\n  [verification]")

            previous_hash_match = previous_hash == block.previous_hash
            print(indent(f"previous block hash : " + ("matching" if previous_hash_match else "not matching"), 2))

            expected_suffix = block.expected_suffix()
            print(indent(f"expected suffix : {hex(expected_suffix)} on {block.suffix_length()} bits", 2))
            previous_hash = hash_string(block.hash_content())
            suffix = previous_hash & mask(block.suffix_length())
            current_hash_valid = suffix == expected_suffix
            print(indent(f"hash suffix : " + ("valid" if current_hash_valid else f"invalid (suffix = {hex(suffix)})"), 2))

            if not previous_hash_match or not current_hash_valid:
                print(f"-> blockchain proved invalid at block {i}")
                return

        print("-> blockchain is valid")

    def __str__(self):
        return "\n".join(map(str, self.blocks))
