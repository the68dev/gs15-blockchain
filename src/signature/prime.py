import secrets


def find_prime(n=512, nb_it=5):  # find a prime number
    key = secrets.randbits(n) | 1

    while not (test_rabin_miller(key, nb_it) and test_rabin_miller(2 * key + 1, nb_it)):
        key += 2

    return 2 * key + 1


def expo_rapide(a, exp, mod):
    if a == 0:
        return 0

    result = 1
    while exp > 0:
        if exp % 2 == 1:
            result = (result * a) % mod
        exp = exp // 2
        a = (a * a) % mod
    return result


def test_rabin_miller(n, nb_it):
    if n != 2 and n % 2 == 0:
        return False
    elif n < 4:
        return True

    r = 0
    d = n - 1
    while d % 2 == 0:
        r += 1
        d //= 2
    for i in range(nb_it):
        a = secrets.randbelow(n - 3) + 2
        x = expo_rapide(a, d, n)
        if x == 1 or x == n - 1:
            continue
        for _ in range(r - 1):
            x = pow(x, 2, n)
            if x == n - 1:
                break
        else:
            return False
    return True


def find_generator(p: int):
    p1 = 2
    p2 = (p - 1) // p1

    g = secrets.randbelow(p - 2) + 2
    while expo_rapide(g, (p - 1) // p1, p) == 1 or expo_rapide(g, (p - 1) // p2, p) == 1:
        g = secrets.randbelow(p - 2) + 2

    return g


if __name__ == '__main__':
    p = 2603036220325273937737364001987095876475828962891183092299876879955733627283707760206963052150864160908858761663745030272166998737994842597239705321188407
    p1 = 8012138386326005822435394295121270407257775055936705984049836150337361735168829172441817833059798500026980527144994474560931324127149389347525234223705073
    print("started")
    while True:
        prime = find_prime(4096)
        print(prime)
        with open("prime.txt", 'a')as file:
            file.write(str(prime) + '\n')
