import math
import secrets
from typing import Tuple

from src.hash.bob_l_eponge import hash_string
from src.math.modularutils import modular_inverse, pgcd
from src.signature.prime import find_prime, find_generator, expo_rapide
from src.signature.signature import Signature


class ElGamalEnc(Signature):
    """
    public_key = (p, g, h)
    private_key = (x)
    """

    def __init__(self, key_size: int = 512):
        self.p = find_prime(key_size)
        self.g = find_generator(self.p)
        x = secrets.randbelow(self.p - 2) + 1
        y = expo_rapide(self.g, x, self.p)

        super().__init__(y, x)

    def sign(self, hach: int):
        while True:
            k = 0
            while pgcd(self.p - 1, k) != 1:
                k = secrets.randbelow(self.p - 4) + 2
            r = expo_rapide(self.g, k, self.p)
            k_minus_1 = modular_inverse(k, self.p - 1)
            s = (k_minus_1 * (hach - self.private_key * r)) % (self.p - 1)
            if s != 0:
                break
        return r, s

    def verify(self, hach: int, signature: Tuple[int, int]) -> bool:
        # signature = (r,s)
        if 0 < signature[0] < self.p and 0 < signature[1] < self.p - 1:
            p = self.p
            r = signature[0]
            s = signature[1]
            v1 = expo_rapide(self.g, hach, p)
            v2 = (expo_rapide(self.public_key, r, p) * expo_rapide(r, s, p)) % p
            if v1 == v2:
                return True
        return False

    def __getstate__(self):
        """Return state values to be pickled."""
        return super().__getstate__(), self.p, self.g

    def __setstate__(self, state):
        """Restore state from the unpickled state values."""
        keys, self.p, self.g = state
        super().__setstate__(keys)

    def __str__(self):
        return f"{super().__str__()}, p = {self.p}, g = {self.g}"


if __name__ == '__main__':
    rand = 'Ranit'
    elgam = ElGamalEnc(100)
    hach = hash_string(rand)
    sign = elgam.sign(hach)
    print(elgam.verify(hach, sign))
