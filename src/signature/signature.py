from abc import ABC, abstractmethod

class Signature(ABC):
    def __init__(self, public: int = -1, private: int = -1):
        self.private_key = private
        self.public_key = public

    @abstractmethod
    def sign(self, hachis: int):
        pass

    @abstractmethod
    def verify(self, hach: int, signature):
        pass

    def __getstate__(self):
        """Return state values to be pickled."""
        return self.public_key, self.private_key

    def __setstate__(self, state):
        """Restore state from the unpickled state values."""
        self.public_key, self.private_key = state

    def __str__(self):
        return f"public = {hex(self.public_key)}, private = {hex(self.private_key)}"
