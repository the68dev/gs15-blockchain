import math
import secrets

from src.signature.signature import Signature
from src.signature.prime import find_prime, expo_rapide
from src.hash.bob_l_eponge import hash_string
from src.math.modularutils import modular_inverse, pgcd


class RSAEnc(Signature):

    def __init__(self, key_size):
        p, q = 0, 0
        while p == q:
            p = find_prime(key_size)
            q = find_prime(key_size + 3)
        n = p * q
        lam = ((p - 1) * (q - 1)) // math.gcd(p - 1, q - 1)
        e = 0
        while pgcd(e, lam) != 1:
            e = secrets.randbelow(lam - 1) + 1
        d = modular_inverse(e, lam)
        self.n = n

        super().__init__(e, d)

    def sign(self, hachis: int):
        return expo_rapide(hachis, self.private_key, self.n)

    def verify(self, hach: int, signature: int):
        return expo_rapide(signature, self.public_key, self.n) == hach % self.n

    def __getstate__(self):
        """Return state values to be pickled."""
        return super().__getstate__(), self.n

    def __setstate__(self, state):
        """Restore state from the unpickled state values."""
        keys, self.n = state
        super().__setstate__(keys)

    def __str__(self):
        return f"{super().__str__()}, n = {self.n}"

if __name__ == '__main__':
    rand = 'Ranit'
    rsa = RSAEnc(8)
    hach = hash_string(rand)
    sign = rsa.sign(hach)
    print(rsa.verify(hach, sign))
