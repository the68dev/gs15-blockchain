import secrets
from typing import List

from src.math.bytesutils import chunks_of, mask, rotate_left


class Xoshiro:
    state: List[int] = [0] * 4

    def __init__(self, init: int):
        """:arg init a 256 bit number"""
        self.mask = mask(64)
        self.state = chunks_of(init, 256, 64)

    def next(self):
        result = (self.state[0] + self.state[3]) & self.mask
        t = (self.state[1] << 17) & self.mask

        self.state[2] ^= self.state[0]
        self.state[3] ^= self.state[1]
        self.state[1] ^= self.state[2]
        self.state[0] ^= self.state[3]

        self.state[2] ^= t

        self.state[3] = rotate_left(self.state[3], 64, 45)

        return result

    def generate_n_bit_number(self, nb_bits: int):
        """:return a int on *nb_bits* bits"""
        if nb_bits <= 64:
            return self.next() >> (64 - nb_bits)
        else:
            t = 0
            nb_loop = (nb_bits // 64) + 1
            for i in range(nb_loop):
                t = (t << 64) | (self.next())
            return t >> (64 * nb_loop - nb_bits)


if __name__ == '__main__':
    nb=1000
    rand = Xoshiro(secrets.randbits(256))
    t = rand.generate_n_bit_number(nb)
    print(len(bin(t))-2)
    print(bin(t))
    print('0b' + '0'*nb)
