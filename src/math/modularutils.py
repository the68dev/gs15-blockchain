def euclide_etendu(a, b):
    r0 = a
    r1 = b
    x0 = 1
    x1 = 0
    y0 = 0
    y1 = 1
    q1 = r0 // r1
    k = 1
    r2 = 0
    x2 = 0
    y2 = 0

    while r2 != 1:
        k += 1
        r2 = r0 % r1
        q2 = r1 // r2
        x2 = q1 * x1 + x0
        y2 = q1 * y1 + y0
        x0, x1 = x1, x2
        y0, y1 = y1, y2
        r0, r1 = r1, r2
        q1 = q2

    return k, x2, y2


def pgcd(a: int, b: int):
    while b != 0:
        t = b
        b = a % b
        a = t
    return a


def modular_inverse(number: int, mod: int):
    i, xn, yn = euclide_etendu(number, mod)
    p = 1 if i % 2 == 0 else -1
    return (p * xn) % mod


if __name__ == '__main__':
    print(modular_inverse(39, 47))
    print(modular_inverse(5, 18))
    print(modular_inverse(5, 19))
