from typing import List
from math import ceil


def mask(bits: int):
    return (1 << bits) - 1


def int_to_bytes(data: int, byte_per_chunk=8):
    """Convertit un bloc en suite d'octets"""
    return bytes([(data >> ((byte_per_chunk - i - 1) * 8)) & 0xFF for i in range(byte_per_chunk)])


def bytes_to_int(data: bytes, expected_size=8) -> int:
    """Generateur de blocks de n octets à partir de la chaine de caractère donnée"""
    value = 0
    data += bytes([0] * (expected_size - len(data)))
    for j in range(expected_size):
        value = (value << 8) + data[j]
    return value


def chunks_of(data: int, input_length=128, bit_size=16) -> List[int]:
    """Convertit un entier en liste de sous partie de ce dernier"""
    n = ceil(input_length / bit_size)
    chunk_mask = mask(bit_size)
    return [(data >> ((n - i - 1) * bit_size)) & chunk_mask for i in range(n)]


def rotate_left(data: int, input_length: int, rotation: int) -> int:
    left = data << rotation
    right = data >> (input_length - rotation)
    return (left | right) & mask(input_length)


def rotate_right(data: int, input_length: int, rotation: int) -> int:
    left = data << (input_length - rotation)
    right = data >> rotation
    return (left | right) & mask(input_length)
