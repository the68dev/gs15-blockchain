from src.cli.blockchain_cli import blockchain_cli
from src.cli.command import Command, CLI
from src.cli.context import Context
from src.cli.crypt_cli import crypt_cli
from src.cli.hash_cli import hash_cli


def open_blockchain(context: Context):
    try:
        blockchain_cli.loop(context, "blockchain")
    except KeyboardInterrupt:
        pass
    return True


def open_hash(context: Context):
    try:
        hash_cli.loop(context, "hash")
    except KeyboardInterrupt:
        pass
    return True


def open_crypt(context: Context):
    try:
        crypt_cli.loop(context, "crypt")
    except KeyboardInterrupt:
        pass
    return True


if __name__ == "__main__":
    CLI([
        Command("crypt",
                "Access crypt sub-menu",
                method=open_crypt,
                args=[]
                ),
        Command("blockchain",
                "Access blockchain sub-menu",
                method=open_blockchain,
                args=[]
                ),

        Command("hash",
                "Access hash sub-menu",
                method=open_hash,
                args=[]
                ),
    ]).loop(Context.load())
